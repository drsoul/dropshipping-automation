
var wd = require('./wd_singleton.js');
var sys = require('./sys.js');
var mongo = require('./mongo.js');
var eventEmit = require('./event_emit_singleton.js');

var apishops = module.exports = {

	elementWaitTime: 20000,

	login:function(callback){
		sys.log('function - login start. Arguments: ', arguments);

		wd.i()
		    .url('http://www.apishops.com/')
		    .click('*[class="loginlink"]')
		    .setValue('*[name="login"]','drsoul')
		    .setValue('*[name="password"]','247356')
		    .click('*[type="submit"]')
		    .then(function(){
		    	callback();
		    });
	},
	getProducts:function(callback){
		sys.log('function - getProducts start. Arguments: ', arguments);

		// apishop
		wd.i()
	    .url('http://www.apishops.com/Webmaster/WebsiteGroup/WebsiteGroupList.jsp')
	    .click('input[name=plate_radio]')
	    .click('td.form_text1 a.ajax_link')
			.then(function(res){
				apishops.changeWindowOn1(res, callback);
			});
		sys.log('getProducts END');
	},
	changeWindowOn1:function(res, callback){
		sys.log('function - changeWindowOn1 start. Arguments: ', arguments);

		wd.i()
			.windowHandles()
			.then(function(res){

				sys.log('windowHandles is:', res.value);
				sys.log('focus to window:', res.value[res.value.length-1]);

				wd.i()
					.window(res.value[res.value.length-1])
					.then(apishops.searchAllProduct(callback));
			})
	},
	searchAllProduct:function(callback){
		sys.log('function - searchAllProduct start. Arguments: ', arguments);

		wd.i()
			.click('input#land')
			.click('input.product-search-button')
			.pause(15000)
			.getText('div.product-item-articul')
			.then(function(res){

				sys.log('all products:', res);
				sys.log('products count:', res.length);

				var productsArr = [];
				for(i in res){
					productsArr.push({'apishopsId':res[i].replace('#', '')});
				}

				sys.log(productsArr);
				mongo.insertMany('products', productsArr);
				//mongaIteract(productsArr);
				callback();
			});
	},
	createProduct:function(apishopsId){
		sys.log('function - createProduct start. Arguments: ', arguments);

		// apishop
		wd.i()
		    //.url('http://www.apishops.com/Webmaster/WebsiteGroup/WebsiteGroupList.jsp')
				.url('http://www.apishops.com/Webmaster/WebsiteGroup/WebsiteGroupList.jsp?groupId=161107')
		    .click('input[name=plate_radio]')
		    .click('td.form_text1 a.ajax_link')
				.then(function(res){

		    	sys.log('click return is:', res);

		    	wd.i()
		    		.windowHandles()
						.then(function(res){

		    			sys.log('windowHandles is:', res.value);
		    			sys.log('focus to window:', res.value[res.value.length-1]);

		    			wd.i()
		    				.window(res.value[1])//res.value.length-1
								.then(function(){
									wd.i()
										.waitForExist('#text2seek', apishops.elementWaitTime)
										.then(function(){
											sys.log("waitForExist('#text2seek') END.");
											apishops.searchProduct(apishopsId);
										});
								});
		    		})
		    });
	},
	searchProduct:function(apishopsId){
		sys.log('function - searchProduct start. Arguments: ', arguments);

		wd.i()
			.setValue('#text2seek', apishopsId)
			.click('input.product-search-button')
			.then(function(){
				sys.log('click OK!');
			})
			.pause(3000)
			.then(function(){
				sys.log('pause OK!');
			})
			.execute(function(apishopsId) {
				// browser context - you may not access neither client nor console
				// non able use sys object in this context
				console.log('execute: showInfo() by apishopsId: '+apishopsId);
				showInfo(null,"standard", apishopsId, -1,"adaptive");
				fill(apishopsId,-1,"adaptive");
			}, apishopsId)
			.then(function(ret){
				sys.log('execute OK!');

				//pause for to avoid selenium crash while change windows
				wd.i()
					.pause(2000)
					.then(function(){
						apishops.changeWindowOn0(ret, apishopsId);
					});
			});
	},
	changeWindowOn0:function(ret, apishopsId) {
		sys.log('function - changeWindowOn0 start. Arguments: ', arguments);

		wd.i()
			.windowHandles()
			.then(function(res){
					sys.log('res: ', res);
					wd.i()
						.window(res.value[0])
						.then(function(){
							wd.i()
								.waitForExist('#wsp_url_1', apishops.elementWaitTime)
								.then(apishops.createProductOnApiShops(apishopsId));
						});
			});
			// node.js context - client and console are available
			sys.log('ret: ', ret);
	},
	createProductOnApiShops:function(apishopsId){
		sys.log('function - createProductOnApiShops start. Arguments: ', arguments);

		wd.i()
			.setValue('#wsp_url_1', 'proddr'+apishopsId)
			//.setValue('[name=systemDomainId]', 19) //19 = bigshop.ru
			.selectByValue('[name=systemDomainId]', 19) //19 = bigshop.ru
			.click('#wsp_submit')
			.then(function(){
				wd.i()
					.waitForExist('.table2', apishops.elementWaitTime)
					.then(function(){

						mongo.init(function(db){
							mongo.setProductAttr(
								db,
								{'apishopsCreateProd':1},
								apishopsId,
								function(err){
									sys.log('Monga err: ', err);
									sys.log("Mongodb - apishopsId:"+apishopsId+" - {'apishopsCreateProd':1} - save");
									apishops.productOnStoreTest(apishopsId);
									db.close();
								}
							);
						});
					});
			});

	},
	productOnStoreTest:function(apishopsId){
		sys.log('function - productOnStoreTest start. Arguments: ', arguments);

		wd.i()
			.waitForExist('.table2', apishops.elementWaitTime)
			.then(function(){
				wd.i()
					.getText('.table2')
					.then(function(product){
						sys.log('PRODUCT: ', product);

						if(product[9] == 'да'){
							sys.log('name: ', product[1]);
							sys.log('cat: ', product[10]);

							apishops.productPresentSave(apishopsId, product);
						}else{
							apishops.productNotPresentSave(apishopsId);
						}
					});
			});
	},
	productNotPresentSave:function(apishopsId){
		sys.log('Product not present!');
		mongo.init(function(db){
			mongo.setProductAttr(
				db,
				{'present': 0},
				apishopsId,
				function(err){
					sys.log('err: ', err);
					sys.log('Monga product save end.');
					db.close(true, function(){
						eventEmit.i().emit('nextNonProcessedProduct');
					});
				}
			);
		});
	},
	productPresentSave:function(apishopsId, product){
		sys.log('function - setProductAttrs start. Arguments: ', arguments);

		mongo.init(function(db){
			wd.i()
				.getUrl()
				.then(function(url) {
						sys.log('URL: ', url);

						wd.i()
							.getText('#center3')
							.then(function(site){

								sys.log('product object: ', product);
								sys.log('SITE:', site);

								var productForMongaSave = {
									//delete info in brakets and delete id, text after comma, '\n', spaces
									'name':product[1]
										.replace(/ ?#[0-9]*$/, '').replace(/ ?\(.*\)/, '')
										.replace(/,.*/, '').replace('\n', '').replace(/ *$/,''),
									//get info int brakets
									'name1':(match = product[1].match('(?:\\()(.*)(?=\\))'))?match[1]:null,
									//get info after comma without brakets content
									'name2':(match = product[1].replace(/ ?\(.*\)/, '')
										.match('(?:, ?)(.*)'))?match[1].replace(/ *$/, ''):null,
									'cat':product[10],
									'vendorCode':product[2],
									'date_add':product[3],
									'model':product[4],
									'manufacturer':product[5],
									//get money or null
									'priceRU':(match = product[6].match('(?:RU: )([0-9.]*)(?=(\\n|))'))?match[1]:null,
									'priceBY':(match = product[6].match('(?:BY: )([0-9.]*)(?=(\\n|))'))?match[1]:null,
									'priceKZ':(match = product[6].match('(?:KZ: )([0-9.]*)(?=(\\n|))'))?match[1]:null,
									'present': 1,
									//get siteId in apishop or null
									'siteIdInApiShops':(match = url.match('(?:siteId=)([0-9]*)($|&)'))?match[1]:null,
									'site': site,
								};
								sys.log('porduct for monga save: ', productForMongaSave);

								mongo.setProductAttr(
									db,
									productForMongaSave,
									apishopsId,
									function(err){
										sys.log('err: ', err);
										sys.log('Monga product save end.');
										db.close(true, function(){
											eventEmit.i().emit('nextNonProcessedProduct');
										});
									}
								);

							});
				});
		});
	},
	end:function(callback){
		sys.log('function - end start. Arguments: ', arguments);

		wd.i()
			.end();
	}
}
