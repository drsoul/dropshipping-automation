var wd = require('./wd_singleton.js');
var sys = require('./sys.js');
var mongo = require('./mongo.js');
var eventEmit = require('./event_emit_singleton.js');

var wordstat = module.exports = {

	pages: 39,
	keyPhraseArr: [],
	//iteratePage: 1,
	//pageLoadDelay: 2000,
	waitLoadContent: 5000,

	login:function(callback){
		sys.log('function - login start. Arguments: ', arguments);

		wd.i()
	    .url('https://wordstat.yandex.ru')
			.click('td.b-head-userinfo__entry a')
	    .setValue('input#b-domik_popup-username.b-form-input__input','drsoul')
	    .setValue('input#b-domik_popup-password.b-form-input__input','24735612')
	    .pause(2000)
			.then(function(){
				wd.i()
			    .keys('\uE007')//works only enter button, not click to button
					.pause(3000)
					.then(function(){
						if(typeof callback == 'function'){
							callback();
						}
					});
			});
	},
	getPhrasesByPage:function(doc, page){
		sys.log('function - getPhrasesByPage start. Arguments: ', arguments);

		var name = '';
		/*
		//2-3 words from product name
		try{
			var nameArr = doc.name.split(' ', 3);
			if(nameArr[1].length < 4){
				name = nameArr.join(' ');
			}else{
				name = nameArr[0]+' '+nameArr[1];
			}
		}catch(err){
			sys.log('Error nameArr', err);
			name = doc.name;
		}
		*/

		//full product name
		name = doc.name;

		wd.i()
			.url('https://wordstat.yandex.ru/#!/?page='+page+'&words='+name)
			.pause(wordstat.waitLoadContent)
			.then(function(){
				wd.i()
					.isExisting('div.b-word-statistics__including-phrases td.b-word-statistics__td-phrase')
					.then(function(isExisting){

						 page++;

						 if(isExisting === true){
							 wordstat.getAndSumPhrases(doc, page);
						 }else{
							 if(page == 1){
								 //not display phrases for 1 page wordstat for this product
								 //go next product
							 }else{
								 page = 1;
								 //save tmp phrases and go next product
							 }
							 wordstat.savePhrases(doc);
						 }
					});
			});
	},
	getAndSumPhrases:function(doc, page){
		sys.log('function - getAndSumPhrases start. Arguments: ', arguments);

	 	wd.i()
	 		.getText('div.b-word-statistics__including-phrases td.b-word-statistics__td-phrase')
	 		.then(function(phrase){
	 			//sys.log('phrase: ', phrase);

	 			wd.i()
	 				.getText('div.b-word-statistics__including-phrases td.b-word-statistics__td-number')
	 				.then(function(number){
	 					//sys.log('number: ', number);
						var tmpKeyPhraseArr = {};
						if(phrase.length > 0){

							var phraseClear = [];
							try{
								phraseClear = phrase.map(function(val) {
								  return val.replace(/\+/g, '');
								});
							}catch(err){
									sys.log('Error phraseClear: ', err);
									phraseClear.push(phrase.replace(/\+/g, ''));
							}

		 					tmpKeyPhraseArr = {
			 					'phrase': phraseClear,
			 					'number': number
							}
		 					wordstat.keyPhraseArr.push(tmpKeyPhraseArr);
						}
	 					sys.log('wordstat.keyPhraseArr: ', wordstat.keyPhraseArr);

	 					if(page === wordstat.pages){
							wordstat.savePhrases(doc);
	 					}else{
							sys.log('iterate: ', page, doc.name);
							eventEmit.i().emit('wordtatsNextPageIterate', doc, page);
	 					}
	 				});
	 		});
	},
	savePhrases:function(doc){
		sys.log('function - savePhrases start. Arguments: ', arguments);

		var wordstatNotEmpty = 0;
		if(wordstat.keyPhraseArr.length > 0){
			wordstatNotEmpty = 1;
		}

		var objForSave = {
			wordstatGrabed: 1,
			wordstatNotEmpty: wordstatNotEmpty,
			wordstatPhrasesAndCount: wordstat.keyPhraseArr
		}

		var objForSaveAdditionKeyPhraseByFullProductName = {
			wordstatAddingKeyPhrasesByFullProductName: 1,
			wordstatPhrasesAndCount: doc.wordstatPhrasesAndCount.concat(wordstat.keyPhraseArr)
		}

		mongo.updateOne(
			'products',
			{apishopsId: doc.apishopsId},
			objForSaveAdditionKeyPhraseByFullProductName,
			function(){
				wd.i()
					.pause(2000)
					.then(function(){
						sys.log('objForSave: ', objForSave);
						//sys.log('objForSaveTest: ', objForSaveTest);
						wordstat.keyPhraseArr = [];
						eventEmit.i().emit('nextNonGrabedWordstatProduct');
					});
			}
		);
	}
}
