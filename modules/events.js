
var wd = require('./wd_singleton.js');
var sys = require('./sys.js');
var mongo = require('./mongo.js');
var wordstat = require('./wordstat.js');
var apishops = require('./apishops.js');
var eventEmit = require('./event_emit_singleton.js');
var directYa = require('./direct_ya.js');

var events = module.exports = {

  getProducts:function(){
    //getProducts
    apishops.login(function(){
      apishops.getProducts(function(){
        apishops.end();
      });
    });
  },

  directYaAddAdLoop:function(){
    //direct.yandex.ru add ads
    directYa.login(function(){
      directYaAddAdLoop();
    });

    function directYaAddAdLoop(){
      //register event listeners
      eventEmit.i().on('nextNonAddedAd', function(){
        console.log('Event - nextNonAddedAd occurred.');

        //directYa createAd
        mongo.init(function(db){
          mongo.getOne({
              directYaEndOfAddKeyphrasesToAds:{$ne:1},
              wordstatNotEmpty:1,
              name:{$exists:true},
              site:{$exists:true}
            },
            function(doc){
              directYa.createAd(doc);
            }
          );
        });
      });

      //trigger event
      eventEmit.i().emit('nextNonAddedAd');
    }
  },
  apishopsProductCreateLoop:function(){
    //create apishops product
    apishops.login(function(){
      apishopsProductCreateLoop();
    });

    function apishopsProductCreateLoop(){
      //register event listeners
      eventEmit.i().on('nextNonProcessedProduct', function(){
        console.log('Event - nextNonProcessedProduct occurred.');

        //apishops createProduct
        mongo.init(function(db){
          mongo.getNonProcessedProduct(
            db,
            function(doc){
              //createProductWrapper(doc);
              apishops.createProduct(doc.apishopsId);
            }
          );
        });
      });

      //trigger event
      eventEmit.i().emit('nextNonProcessedProduct');
    }
  },
  wordstatGrabPhrases:function(){
    //create apishops product
    wordstat.login(function(){
      wordstatGrabPhrasesLoop();
    });

    function wordstatGrabPhrasesLoop(){
      //register event listeners
      eventEmit.i().on('nextNonGrabedWordstatProduct', function(){
        console.log('Event - nextNonGrabedProduct occurred.');
        grabAndSaveProduct();
      });

      eventEmit.i().on('wordtatsNextPageIterate', function(doc, pageIterate){
        console.log('Event - wordtatsNextPageIterate occurred.');

        if(pageIterate == undefined){
          pageIterate = 1;
        }
        wordstat.getPhrasesByPage(doc, pageIterate);
      });
      //first run
      grabAndSaveProduct();
    }

    function grabAndSaveProduct(){

      var whereObj = {
        wordstatGrabed:{$ne:1},
        name:{$exists:true},
        site:{$exists:true}
      }

      var whereObjForSearchKeyPhrasesByFullProductName = {
        wordstatNotEmpty:1,
        wordstatAddingKeyPhrasesByFullProductName: {$ne:1},
        name:{$exists:true},
        site:{$exists:true}
      }

      mongo.init(function(db){
        mongo.getOne(
          whereObjForSearchKeyPhrasesByFullProductName,
          function(doc){
            //first run
            wordstat.getPhrasesByPage(doc, 1);
          }
        );
      });
    }
  }
}
