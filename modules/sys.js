//this helper object
var sys = module.exports = (function(){

	return {
		elementWaitTime: 600000,
		logEnable: 1,
		log:function(){
			if(sys.logEnable == 1){
				if(arguments.length > 1){
					for(i in arguments){
						console.log(arguments[i]);
					}
				}else{
					console.log(arguments[0]);
				}
			}
		},
		transliterate: function(wordOnKirillic){
			var a = {"Ё":"YO","Й":"I","Ц":"TS","У":"U","К":"K","Е":"E","Н":"N","Г":"G","Ш":"SH","Щ":"SCH",
				"З":"Z","Х":"H","Ъ":"","ё":"yo","й":"i","ц":"ts","у":"u","к":"k","е":"e","н":"n","г":"g",
				"ш":"sh","щ":"sch","з":"z","х":"h","ъ":"","Ф":"F","Ы":"I","В":"V","А":"a","П":"P","Р":"R",
				"О":"O","Л":"L","Д":"D","Ж":"ZH","Э":"E","ф":"f","ы":"i","в":"v","а":"a","п":"p","р":"r",
				"о":"o","л":"l","д":"d","ж":"zh","э":"e","Я":"Ya","Ч":"CH","С":"S","М":"M","И":"I","Т":"T",
				"Ь":"","Б":"B","Ю":"YU","я":"ya","ч":"ch","с":"s","м":"m","и":"i","т":"t","ь":"","б":"b","ю":"yu",
				" ":"-"};

			return word.split(wordOnKirillic).map(function (char) {
				return a[char] || '';
			}).join("");

		},
		getInitialChars:function(text, countChars){
			if(text === null){
				return null;
			}
			return text.substr(0,
				text.length - (text.length - countChars));
		},
		lowerCaseNonFirstCharsInWords:function(text){

			return text.split(' ').map(function(word){
				var normalizeWord = '';
				for(i = 0; i < word.length; i++){
					if(i > 0){
						normalizeWord += word.charAt(i).toLowerCase();
					}else{
						normalizeWord += word.charAt(i);
					}
				}
				return normalizeWord;
			}).join(' ');
		}
	}
})();
