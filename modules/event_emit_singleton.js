
//singleton EventEmitter client
module.exports = (function () {

    var instance;

    function createInstance() {

      const EventEmitter = require('events');
      const util = require('util');

      util.inherits(MyEmitter, EventEmitter);
      const myEmitter = new MyEmitter();

      // create EventEmitter
      function MyEmitter() {
        EventEmitter.call(this);
      }

      return myEmitter;
    }


    return {
        i: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();
