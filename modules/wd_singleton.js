//singleton webdriverio client
module.exports = (function () {

    var instance;

    function createInstance() {

		var webdriverio = require('webdriverio');
	    var client = webdriverio.remote({
	        desiredCapabilities: {
	            browserName: 'firefox',
              coloredLogs: true,
              //max_script_run_time: 0
				}
	    }).init();

        return client;
    }

    return {
        i: function () {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();
