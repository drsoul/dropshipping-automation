
var wd = require('./wd_singleton.js');
var sys = require('./sys.js');
var mongo = require('./mongo.js');
var eventEmit = require('./event_emit_singleton.js');
var romanizeNonKirillicAndKazakh = require('./romanize_non_kirillic_and_kazakh.js');


var directYa = module.exports = {

  login:function(callback){
		sys.log('function - login start. Arguments: ', arguments);

    /*
    //popup auth
		wd.i()
        .url('https://direct.yandex.ru/')
        .waitForExist('a.button.button_theme_promo', sys.elementWaitTime)
        .then(function(){
          wd.i()
            .click('a.button.button_theme_promo')
            .setValue('input#b-domik_popup-username.b-form-input__input','drsoul')
            .setValue('input#b-domik_popup-password.b-form-input__input','24735612')
            //.pause(1000)
            .keys('\uE007')//works only enter button, not click to button
            //.pause(2000)
            .then(function(){
              callback();
            });
        });
    */

    //special page for auth
    wd.i()
      //.url('https://passport.yandex.ru/auth?retpath=https%3A%2F%2Fdirect.yandex.ru%2Fregistered%2Fmain.AWJ-ox0IUSlXpFF7.pl%3Fauthredirlevel%3D1461445712.0%26cmd%3DshowCamp%26cid%3D16900958')
      .url('https://passport.yandex.ru/auth?retpath=https%3A%2F%2Fdirect.yandex.ru')
      //.setValue('input#login', 'drsoul')
      //.setValue('input#passwd', '24735612')
      .setValue('input#login', 'fenixionrise')
      .setValue('input#passwd', '24735612')
      .click('button[type=submit]')
      .then(function(){
        wd.i()
          .pause(5000)
          .then(function(){
            callback();
          });
      });
	},
  createAd:function(product){
    sys.log('function - createAd start. Arguments: ', arguments);

    wd.i()
      //main acc company
      //.url('https://direct.yandex.ru/registered/main.mTgvGS8SRG3OJUsb.pl?cmd=showCamp&cid=18259006')
      //second acc company
      .url('https://direct.yandex.ru/registered/main.pl?cmd=showCamp&cid=18312166')
      .waitForExist('form.b-campaign-info__main-form', sys.elementWaitTime)
      .then(function(){

        /*
        var keywords = product.name;
        keywords += (typeof product.name1 == "undefined" || product.name1 == null)
          ?'':'\n'+product.name1;
        keywords += (typeof product.model == "undefined" || product.model == null
          || product.model == 'Не задано')?'':'\n'+product.model;
        */

        var indexOfKeyphrasesArr
          = directYa.computeIndexAddedElementOfKeyphrasesArrToAds(product);

        var newIndexAndBlockOfKeyphrases
          = directYa.createNewIndexAndBlockOfKeyphrases(product, indexOfKeyphrasesArr);

        //end of keyphrases. Save and emit next event
        if(newIndexAndBlockOfKeyphrases.directYaEndOfAddKeyphrasesToAds === 1){
          return directYa.saveAddAdResult(product, newIndexAndBlockOfKeyphrases);
        }

        wd.i()
          .submitForm('form.b-campaign-info__main-form')
          .setValue('div.b-edit-group-header__control-content input.input__control',
            sys.getInitialChars(product.name, 27)+' - '
            +newIndexAndBlockOfKeyphrases.directYaIndexOfKeyPhrasesArr
          )
          .setValue('input[name="title"]',
            sys.lowerCaseNonFirstCharsInWords(sys.getInitialChars(product.name, 33))
          )
          .setValue('textarea[name="body"]', 'Купить!')
          .setValue('input[name="href"]', product.site)
          .click('textarea.b-phrases-input__phrases-textarea')
          .execute(function(keyPhrasesBlock) {
              // browser context - you may not access client or console
              var res = document.getElementsByTagName('textarea')[1].value
                = keyPhrasesBlock;
              return res;
          }, newIndexAndBlockOfKeyphrases.keyPhrasesBlock).then(function(ret) {
              // node.js context - client and console are available
              sys.log('JS script exec: ', ret);
          })
          .pause(3000)
          .then(function(){
            directYa.submitFormAndSaveResult(product, newIndexAndBlockOfKeyphrases);
          });
      });

  },
  submitFormAndSaveResult:function(product, newIndexAndBlockOfKeyphrases){
    wd.i()
      .waitForExist('button.p-multiedit2__next-button_type_text', sys.elementWaitTime)
      .then(function(){
        wd.i()
          .click('button.p-multiedit2__next-button_type_text')
          .pause(3000)
          .then(function(){
            wd.i()
              .waitForExist('button.p-price-multiedit__submit-button',
                sys.elementWaitTime)
              .then(function(){
                wd.i()
                  .click('button.p-price-multiedit__submit-button')
                  .waitForExist('form.b-campaign-info__main-form', sys.elementWaitTime)
                  .then(function(){
                    directYa.saveAddAdResult(product, newIndexAndBlockOfKeyphrases);
                  });
              });
          });
      });
  },
  saveAddAdResult:function(product, newIndexAndBlockOfKeyphrases){
    mongo.updateOne(
      'products',
      {apishopsId: product.apishopsId},
      {
        directYaIndexOfKeyPhrasesArr:
          newIndexAndBlockOfKeyphrases.directYaIndexOfKeyPhrasesArr,
        directYaEndOfAddKeyphrasesToAds:
          newIndexAndBlockOfKeyphrases.directYaEndOfAddKeyphrasesToAds
      },
      function(){
        eventEmit.i().emit('nextNonAddedAd');
      }
    );
  },
  computeIndexAddedElementOfKeyphrasesArrToAds: function(product){

    //compute indexAddedElementOfArrToAds
    var indexAddedElementOfArrToAds = 0;
    if(typeof product.directYaIndexOfKeyPhrasesArr == 'undefined'
      || product.directYaIndexOfKeyPhrasesArr == null){
      indexAddedElementOfArrToAds = 0;
    }else{
      indexAddedElementOfArrToAds
        = product.directYaIndexOfKeyPhrasesArr;
    }

    return indexAddedElementOfArrToAds;
  },
  //@return object (keyPhrasesBlock, directYaNewIndexOfKeyPhrasesArr,
  // directYaEndOfAddKeyphrasesToAd)
  createNewIndexAndBlockOfKeyphrases:function(product, indexOfKeyphrasesArr){

    //create keyphrases block
    var tmpPhraseArr = [];
    var keyPhrasesBlock = '';
    var directYaNewIndexOfKeyPhrasesArr = 0;
    if(indexOfKeyphrasesArr){
      directYaNewIndexOfKeyPhrasesArr = indexOfKeyphrasesArr;
    }
    var directYaEndOfAddKeyphrasesToAds = 0;

    for(var i = 0; i <= 3; i++){
      try{

        if(typeof product.wordstatPhrasesAndCount[directYaNewIndexOfKeyPhrasesArr]
        !== 'undefined'){

          tmpPhraseArr
            = product.wordstatPhrasesAndCount[directYaNewIndexOfKeyPhrasesArr].phrase;

          sys.log('directYaNewIndexOfKeyPhrasesArr: ', directYaNewIndexOfKeyPhrasesArr);
          directYaNewIndexOfKeyPhrasesArr++;
          sys.log('directYaNewIndexOfKeyPhrasesArr after inc: ', directYaNewIndexOfKeyPhrasesArr);

          sys.log('tmpPhraseArr: ', tmpPhraseArr);

          for(index in tmpPhraseArr){
            keyPhrasesBlock += romanizeNonKirillicAndKazakh(tmpPhraseArr[index])+'\n';
          }
        }else{
          directYaEndOfAddKeyphrasesToAds = 1;
        }
      }catch(err){
        sys.log('Error in directYa.createNewIndexAndBlockOfKeyphrases(): ', err);
      }
    }

    sys.log('keyPhrasesBlock: ', keyPhrasesBlock);
    sys.log('directYaNewIndexOfKeyPhrasesArr: ', directYaNewIndexOfKeyPhrasesArr);
    sys.log('directYaEndOfAddKeyphrasesToAds: ', directYaEndOfAddKeyphrasesToAds);

    return {
      'keyPhrasesBlock':keyPhrasesBlock,
      'directYaIndexOfKeyPhrasesArr':directYaNewIndexOfKeyPhrasesArr,
      'directYaEndOfAddKeyphrasesToAds':directYaEndOfAddKeyphrasesToAds
    }
  }

}
