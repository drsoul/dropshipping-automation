
var sys = require('./sys.js');

var mongo = module.exports = {
	db: '',
	init:function(callback){
		sys.log('function - init start. Arguments: ', arguments);
		//lets require/import the mongodb native drivers.
		var mongodb = require('mongodb');

		//We need to work with "MongoClient" interface in order to connect to a mongodb server.
		var MongoClient = mongodb.MongoClient;

		// Connection URL. This is where your mongodb server is running.
		var url = 'mongodb://localhost:27017/apishops';

		// Use connect method to connect to the Server
		MongoClient.connect(url, function (err, db) {
			mongo.db = db;
			if (err) {
				console.log('Unable to connect to the mongoDB server. Error:', err);
			} else {
				//HURRAY!! We are connected. :)
				console.log('Connection established to', url);
				//var cursor = mongo.db.collection('products').find({'processed': 0}).limit(1);
				//sys.log(cursor);
				callback(db);
			}
		});
	},
	getNonProcessedProduct:function(db, callback){
		sys.log('function - getNonProcessedProduct start. Arguments: ', arguments);

		var cursor = db.collection('products')
			.find({'processed': {$ne: 1}, 'apishopsCreateProd': {$ne: 1}})
			.limit(1);
		//sys.log('Cursor: ', cursor);
		//throw new Error('!!!!!!!!');
		//Lets iterate on the result
	    cursor.each(function (err, doc) {
			if (err) {
				console.log('Error get getNonProcessedProduct from db: ', err);
			} else {
				console.log('Fetched:', doc);
				//doc.apishopsId
				//apishops.createProduct(doc.apishopsId);
				if(doc !== null){
					callback(doc);
				}else{
					sys.log('doc === null');
				}

			}
			db.close();
	    });

		//db.close();
		//callback();
	},
	getOne:function(where, callback){
		sys.log('function - getOne start. Arguments: ', arguments);

		var cursor = mongo.db.collection('products').find(where).limit(1);
		//Lets iterate on the result
	    cursor.each(function (err, doc) {
			if (err) {
				console.log(err);
			} else {
				console.log('Fetched:', doc);
				//doc.apishopsId
				//apishops.createProduct(doc.apishopsId);
				if(doc !== null){
					callback(doc);
				}else{
					sys.log('doc === null');
				}

			}
			mongo.db.close();
	    });

	},
	setProductAttr:function(db, attr, apishopsId, callback){
		sys.log('function - setProductAttr start. Arguments: ', arguments);

		db.collection('products').updateOne(
			{ "apishopsId" : apishopsId },
			{ $set: attr },
			function(err, results) {
				if(err !== null){
					sys.log('err: ', err);
				}
				sys.log('results: ', results);
				callback();
			}
		);
	},
	updateOne:function(collection, whereObj, updateObj, callback){
		sys.log('function - updateOne start. Arguments: ', arguments);

		mongo.init(function(){
			mongo.db.collection(collection).updateOne(
				whereObj,
				{ $set: updateObj },
				function(err, results) {
					if(err !== null){
						sys.log('err: ', err);
					}
					sys.log('results: ', results);
					if(typeof callback === 'function'){
						callback();
					}
				}
			);
		});
	},
	insertMany:function(collection, insertArrayOfObjects, callback){
		sys.log('function - insertMany start. Arguments: ', arguments);

		mongo.init(function(){
			mongo.db.collection(collection).insertMany(
				insertArrayOfObjects,
				function(err, results) {
					if(err !== null){
						sys.log('err: ', err);
					}
					sys.log('results: ', results);
					if(typeof callback === 'function'){
						callback();
					}
				}
			);
		});
	},

}
